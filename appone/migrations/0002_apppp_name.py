# Generated by Django 2.0.7 on 2021-11-22 07:06

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('appone', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='apppp',
            name='name',
            field=models.TextField(default=django.utils.timezone.now),
            preserve_default=False,
        ),
    ]
